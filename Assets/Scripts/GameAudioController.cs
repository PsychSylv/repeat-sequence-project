using RepeatSequence.Manager;
using UnityEngine;

namespace RepeatSequence.Audio
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip answerRightAudioClip;
        [SerializeField] private AudioClip answerWrongAudioClip;
        [SerializeField] private AudioClip sequenceRightAudioClip;
        [SerializeField] private AudioClip winAudioClip;
        [SerializeField] private AudioClip loseAudioClip;

        private void Start()
        {
            gameManager.SequencePressed += OnSequencePressed;
            gameManager.SequenceFinished += OnSequenceFinished;
            gameManager.GameFinished += OnGameOver;
        }

        private void OnSequencePressed(bool correct)
        {
            if(correct)
            {
                audioSource.PlayOneShot(answerRightAudioClip);
            }
            else
            {
                audioSource.PlayOneShot(answerWrongAudioClip);
            }
        }
        private void OnSequenceFinished()
        {
            audioSource.PlayOneShot(sequenceRightAudioClip);
        }
        private void OnGameOver(bool win)
        {
            if(win)
            {
                audioSource.PlayOneShot(winAudioClip);
            }
            else
            {
                audioSource.PlayOneShot(loseAudioClip);
            }
        }

        private void OnDestroy()
        {
            gameManager.SequencePressed -= OnSequencePressed;
            gameManager.SequenceFinished -= OnSequenceFinished;
            gameManager.GameFinished -= OnGameOver;
        }
    }
}
