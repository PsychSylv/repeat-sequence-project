using System;
using UnityEngine;

namespace RepeatSequence.Menu.StartMenu
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button.Button normalButton;
        [SerializeField] private Button.Button hardButton;

        public event Action NormalPressed;
        public event Action HardPressed;

        private void Start()
        {
            normalButton.ButtonReleased += NormalModeUBEvent;
            hardButton.ButtonReleased += HardModeUBEvent;
        }
        public void NormalModeUBEvent()
        {
            NormalPressed?.Invoke();
        }
        public void HardModeUBEvent()
        {
            HardPressed?.Invoke();
        }
        private void OnDestroy()
        {
            normalButton.ButtonReleased -= NormalModeUBEvent;
            hardButton.ButtonReleased -= HardModeUBEvent;
        }
    }
}
