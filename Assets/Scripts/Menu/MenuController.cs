using UnityEngine;

namespace RepeatSequence.Menu
{
    public abstract class MenuController : MonoBehaviour
    {
        public virtual void OpenMenu()
        {
            gameObject.SetActive(true);
        }
        public virtual void CloseMenu()
        {
            gameObject.SetActive(false);
        }
    }
}
