using System;
using UnityEngine;
using RepeatSequence.Button.Type;

namespace RepeatSequence.Button.Sequence
{
    public class SequenceButton : Button
    {
        [SerializeField] private ButtonType buttonType;
        public event Action<ButtonType> ButtonPassed;

        protected override void Start()
        {
            base.Start();
            ButtonReleased += OnButtonGet;
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            ButtonReleased -= OnButtonGet;
        }
        public void OnButtonGet()
        {
            ButtonPassed.Invoke(buttonType);
        }
    }
}
