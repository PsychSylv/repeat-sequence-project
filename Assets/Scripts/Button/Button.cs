using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RepeatSequence.Button
{
    public class Button : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Image buttonImage;
        [SerializeField] private Sprite normalButtonSprite;
        [SerializeField] private Sprite pressedButtonSprite;

        public event Action ButtonClicked;
        public event Action ButtonReleased;

        protected virtual void Start()
        {
            ButtonClicked += SetPressedButtonSprite;
            ButtonReleased += SetNormalButtonSprite;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            ButtonClicked?.Invoke();
        }
        public void OnPointerUp(PointerEventData eventData)
        {
            ButtonReleased?.Invoke();
        }
        public virtual Image GetButtonImage()
        {
            return buttonImage;
        }
        public virtual void SetNormalButtonSprite()
        {
            buttonImage.sprite = normalButtonSprite;
        }
        public virtual void SetPressedButtonSprite()
        {
            buttonImage.sprite = pressedButtonSprite;
        }
        protected virtual void OnDestroy()
        {
            ButtonClicked -= SetPressedButtonSprite;
            ButtonReleased -= SetNormalButtonSprite;
        }
    }
}
