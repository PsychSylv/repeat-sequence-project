using RepeatSequence.Button.Sequence;
using RepeatSequence.Button.Type;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RepeatSequence.Sequencer
{
    public class Sequencer : MonoBehaviour
    {
        [SerializeField] private SequenceButton[] sequenceButtons;

        private int buttonIndex;
        private float maxTime = 3f;
        private float breakTime = 1f;
        private float delayTime;
        private bool isActive;

        public event Action<ButtonType> ButtonInputed;

        private void Start()
        {
            OnSequenceDone(false);

            for (int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].ButtonPassed += GetButtonInput;
            }
        }
        private void OnSequenceDone(bool done)
        {
            if (done)
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
        }
        private void GetButtonInput(ButtonType buttonType)
        {
            if(isActive)
            {
                ButtonInputed?.Invoke(buttonType);
            }
        }
        private IEnumerator DelaySequence(float delayTime, List<ButtonType> buttonTypes)
        {
            yield return new WaitForSeconds(breakTime);

            for (int i = 0; i < buttonTypes.Count; i++)
            {
                buttonIndex = (int)buttonTypes[i];
                sequenceButtons[buttonIndex].SetPressedButtonSprite();
                yield return new WaitForSeconds(delayTime * 0.8f);

                sequenceButtons[buttonIndex].SetNormalButtonSprite();
                yield return new WaitForSeconds(delayTime * 0.2f);
            }
            OnSequenceDone(true);
        }
        public void Reset()
        {
            for (int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].SetNormalButtonSprite();
            }
            StopAllCoroutines();
        }
        public void SimulateSequence(List<ButtonType> buttonTypes)
        {
            OnSequenceDone(false);

            delayTime = maxTime / buttonTypes.Count;
            StartCoroutine(DelaySequence(delayTime, buttonTypes));
        }
        public void Show()
        {
            gameObject.SetActive(true);
        }
        public void Hide()
        {
            gameObject.SetActive(false);
        }
        private void OnDestroy()
        {
            for (int i = 0; i < sequenceButtons.Length; i++)
            {
                sequenceButtons[i].ButtonPassed -= GetButtonInput;
            }
        }
    }
}
