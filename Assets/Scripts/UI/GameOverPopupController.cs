using System;
using UnityEngine;

namespace RepeatSequence.UI.Popup.GameOver
{
    public class GameOverPopupController : PopupController
    {
        [SerializeField] private Button.Button playAgainButton;
        [SerializeField] private Button.Button quitButton;

        public event Action PlayAgainPressed;
        public event Action QuitPressed;

        private void Start()
        {
            playAgainButton.ButtonReleased += PlayAgainUBEvent;
            quitButton.ButtonReleased += QuitUBEvent;
        }
        public void PlayAgainUBEvent()
        {
            PlayAgainPressed?.Invoke();
        }
        public void QuitUBEvent()
        {
            QuitPressed?.Invoke();
        }
        private void OnDestroy()
        {
            playAgainButton.ButtonReleased -= PlayAgainUBEvent;
            quitButton.ButtonReleased -= QuitUBEvent;
        }
    }
}
