using System;
using UnityEngine;

namespace RepeatSequence.UI.Popup.Pause
{
    public class PausePopupController : PopupController
    {
        [SerializeField] private Button.Button continueButton;
        [SerializeField] private Button.Button restartButton;
        [SerializeField] private Button.Button quitButton;

        public event Action ContinuePressed;
        public event Action RestartPressed;
        public event Action QuitPressed;

        private void Start()
        {
            continueButton.ButtonReleased += ContinueUBEvent;
            restartButton.ButtonReleased += RestartUBEvent;
            quitButton.ButtonReleased += QuitUBEvent;
        }
        public void ContinueUBEvent()
        {
            ContinuePressed?.Invoke();
        }
        public void RestartUBEvent()
        {
            RestartPressed?.Invoke();
        }
        public void QuitUBEvent()
        {
            QuitPressed?.Invoke();
        }
        private void OnDestroy()
        {
            continueButton.ButtonReleased -= ContinueUBEvent;
            restartButton.ButtonReleased -= RestartUBEvent;
            quitButton.ButtonReleased -= QuitUBEvent;
        }
    }
}
