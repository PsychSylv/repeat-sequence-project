using UnityEngine;

namespace RepeatSequence.UI.Popup
{
    public abstract class PopupController : MonoBehaviour
    {
        public virtual void OpenPopup()
        {
            gameObject.SetActive(true);
        }
        public virtual void ClosePopup()
        {
            gameObject.SetActive(false);
        }
    }
}
