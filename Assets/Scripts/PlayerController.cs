using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace RepeatSequence.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Image mascot;
        [SerializeField] private Sprite mascotDefault;
        [SerializeField] private Sprite mascotCorrect;
        [SerializeField] private Sprite[] mascotWrong;
        [SerializeField] private Image[] healthIndicators;
        [SerializeField] private GameObject dialogueGameObject;
        [SerializeField] private Text dialogueText;

        [SerializeField] private string[] correctDialogue;
        [SerializeField] private string[] wrongDialogue;

        private int health;
        private int random;
        private float dialogueTime = 2f;

        public void ShowDialogue(bool correct)
        {
            dialogueGameObject.SetActive(true);
            if(correct == true)
            {
                random = Random.Range(0, correctDialogue.Length);
                dialogueText.text = correctDialogue[random];
                mascot.sprite = mascotCorrect;
            }
            else
            {
                random = Random.Range(0, wrongDialogue.Length);
                dialogueText.text = wrongDialogue[random];
                
                random = Random.Range(0, mascotWrong.Length);
                mascot.sprite = mascotWrong[random];

                health--;
                SetHealth();
            }
            StartCoroutine(DelayHideDialogue());
        }
        private IEnumerator DelayHideDialogue()
        {
            yield return new WaitForSeconds(dialogueTime);
            HideDialogue();
        }
        private void HideDialogue()
        {
            dialogueGameObject.SetActive(false);
        }
        private void SetHealth()
        {
            for (int i = 0; i < healthIndicators.Length; i++)
            {
                healthIndicators[i].enabled = i < health;
            }
        }
        public int GetHealth()
        {
            return health;
        }
        public void Show()
        {
            gameObject.SetActive(true);
        }
        public void Hide()
        {
            gameObject.SetActive(false);
        }
        public void Reset()
        {
            health = 3;
            SetHealth();
            mascot.sprite = mascotDefault;
            dialogueGameObject.SetActive(false);
        }
    }
}
