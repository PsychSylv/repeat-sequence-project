using RepeatSequence.Button.Type;
using RepeatSequence.Menu.StartMenu;
using RepeatSequence.Player;
using RepeatSequence.UI.Popup.GameOver;
using RepeatSequence.UI.Popup.Pause;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RepeatSequence.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private StartMenuController startMenu;
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;
        [SerializeField] private Sequencer.Sequencer sequencer;
        [SerializeField] private PlayerController player;

        private List<ButtonType> currentSequence;
        private int SequenceLength;
        private int currentSequenceLength;
        private int maxSequenceLength = 7;
        private int random;
        private bool isPause;
        private bool canPause;
        private bool isHard;

        public event Action<bool> SequencePressed;
        public event Action SequenceFinished;
        public event Action<bool> GameFinished;

        private void Start()
        {
            startMenu.NormalPressed += OnStartNormal;
            startMenu.HardPressed += OnStartHard;

            pausePopup.ContinuePressed += OnResume;
            pausePopup.RestartPressed += OnRestart;
            pausePopup.QuitPressed += OnExit;

            gameOverPopup.PlayAgainPressed += OnRestart;
            gameOverPopup.QuitPressed += OnExit;

            sequencer.ButtonInputed += OnCheckSequence;
        }
        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape) && canPause)
            {
                Pause();
            }
        }

        private void StartGame()
        {
            startMenu.CloseMenu();
            pausePopup.ClosePopup();
            gameOverPopup.ClosePopup();

            ShowGame();
            player.Reset();
            sequencer.Reset();

            currentSequenceLength = 0;
            SequenceLength = 3;
            currentSequence = new List<ButtonType>(new ButtonType[SequenceLength]);

            RandomizeSequence(currentSequence.Count);
            sequencer.SimulateSequence(currentSequence);

            isPause = false;
            canPause = true;
        }
        private void ShowNextSequence()
        {
            if (isHard)
            {
                SequenceLength++;
                currentSequence = new List<ButtonType>(new ButtonType[SequenceLength]);
                RandomizeSequence(currentSequence.Count);
            }
            else
            {
                random = UnityEngine.Random.Range(0, sizeof(ButtonType));
                currentSequence.Add((ButtonType)random);
            }
            sequencer.SimulateSequence(currentSequence);
        }
        private void RandomizeSequence(int length)
        {
            for(int i = 0; i < length; i++)
            {
                random = UnityEngine.Random.Range(0, sizeof(ButtonType));
                currentSequence[i] = (ButtonType)random;
            }
        }
        private void OnCheckSequence(ButtonType buttonType)
        {
            if(currentSequence[currentSequenceLength] != buttonType)
            {
                player.ShowDialogue(false);
                SequencePressed?.Invoke(false);
                currentSequenceLength = 0;
                sequencer.SimulateSequence(currentSequence);

                if (player.GetHealth() <= 0)
                {
                    GameOver();
                    GameFinished?.Invoke(false);
                }
            }
            else
            {
                player.ShowDialogue(true);
                SequencePressed?.Invoke(true);
                currentSequenceLength++;

                if(currentSequenceLength == currentSequence.Count)
                {
                    if (currentSequence.Count == maxSequenceLength)
                    {
                        GameOver();
                        GameFinished?.Invoke(true);
                    }
                    else
                    {
                        SequenceFinished?.Invoke();
                        currentSequenceLength = 0;
                        ShowNextSequence();
                    }
                }
            }
        }
        private void ShowGame()
        {
            player.Show();
            sequencer.Show();
        }
        private void HideGame()
        {
            player.Hide();
            sequencer.Hide();
        }
        private void OnStartNormal()
        {
            isHard = false;
            StartGame();
        }
        private void OnStartHard()
        {
            isHard = true;
            StartGame();
        }
        private void Pause()
        {
            if(!isPause)
            {
                isPause = true;
                pausePopup.OpenPopup();
            }
            else
            {
                OnResume();
            }
        }
        private void OnResume()
        {
            pausePopup.ClosePopup();
            isPause = false;
        }
        private void OnRestart()
        {
            StartGame();
        }
        private void OnExit()
        {
            startMenu.OpenMenu();
            gameOverPopup.ClosePopup();
            pausePopup.ClosePopup();
            HideGame();

            canPause = false;
        }
        private void GameOver()
        {
            gameOverPopup.OpenPopup();
            canPause = false;
        }
        private void OnDestroy()
        {
            startMenu.NormalPressed -= OnStartNormal;
            startMenu.HardPressed -= OnStartHard;

            pausePopup.ContinuePressed -= OnResume;
            pausePopup.RestartPressed -= OnRestart;
            pausePopup.QuitPressed -= OnExit;

            gameOverPopup.PlayAgainPressed -= OnRestart;
            gameOverPopup.QuitPressed -= OnExit;

            sequencer.ButtonInputed -= OnCheckSequence;
        }
    }
}
